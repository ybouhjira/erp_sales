<?php

/**
 * @file
 * Contains the declaration of content types, fields, taxonomy vocabularies 
 * and terms. 
 */

/**
 * Returns content types
 */
function _erp_sales_node_types() {
    $node_types = array(
        'opportunity' => array(
            'name' => t('Opportunity'),
            'description' => t('A business opportunity'),
            'title_label' => t('Name'),
        ),
        'customer' => array(
            'name' => t('Customer'),
            'description' => t('A customer of our company'),
            'has_title' => FALSE,
        )
    );
    foreach ($node_types as &$type) {
        $type['base'] = 'erp_sales';
        $type['locked'] = TRUE;
    }
    return $node_types;
}

/**
 * Returns fields
 */
function _erp_sales_fields() {
    $fields = array(
        // Opportunity fields
        'opportunity_next_action_date' => array('type' => 'date'),
        'opportunity_next_action' => array('type' => 'text'),
        'opportunity_categories' => array(
            'type' => 'taxonomy_term_reference',
            'cardinality' => FIELD_CARDINALITY_UNLIMITED,
            'settings' => array(
                'allowed_values' => array(
                    array(
                        'vocabulary' => 'categories',
                        'parent' => 0,
                    ),
                ),
            ),
        ),
        'opportunity_status' => array(
            'type' => 'list_text',
            'settings' => array(
                'allowed_values' => array(t('New'), t('Qualification'), t('Proposition'), t('Negotiation'), t('Won')),
            ),
        ),
        'opportunity_revenue' => array('type' => 'number_decimal'),
        // CUSTOMER FIELDS =====================================================
        'customer_first_name' => array('type' => 'text'),
        'customer_last_name' => array('type' => 'text'),
        'customer_is_a_company' => array('type' => 'list_boolean'),
        'customer_tags' => array(
            'type' => 'taxonomy_term_reference',
            'cardinality' => FIELD_CARDINALITY_UNLIMITED,
            'settings' => array(
                'allowed_values' => array(
                    array(
                        'vocabulary' => 'customer_tags',
                        'parent' => 0,
                    ),
                ),
            ),
        ),
        'customer_email' => array('type' => 'email'),
        'customer_phonenumber' => array('type' => 'text'),
        'customer_mobilenumber' => array('type' => 'text'),
        'customer_fax' => array('type' => 'text'),
        'customer_address' => array('type' => 'text_long'),
        'customer_url' => array('type' => 'url'),
        'customer_photo' => array('type' => 'image'),
        'customer_title' => array(
            'type' => 'taxonomy_term_reference',
            'settings' => array(
                'allowed_values' => array(
                    array(
                        'vocabulary' => 'titles',
                        'parent' => 0,
                    ),
                ),
            )
        ),
        'customer_job_position' => array('type' => 'text'),
    );

    foreach ($fields as $key => &$field) {
        $field['field_name'] = $key;
    }

    return $fields;
}

/**
 * Returns fields instances
 */
function _erp_sales_instances() {
    $instances = array(
        // Opportunity instances
        'opportunity_next_action_date' => array(
            'label' => t('Next action date'),
            'bundle' => 'opportunity',
            'widget' => array('type' => 'date_popup'),
        ),
        'opportunity_next_action' => array(
            'label' => t('Next action'),
            'bundle' => 'opportunity',
            'widget' => array('type' => 'text_textarea'),
        ),
        'opportunity_categories' => array(
            'label' => t('Categories'),
            'bundle' => 'opportunity',
            'widget' => array('type' => 'autocomplete_deluxe_taxonomy'),
        ),
        'opportunity_status' => array(
            'label' => t('Status'),
            'bundle' => 'opportunity',
            'widget' => array('type' => 'select'),
        ),
        'opportunity_revenue' => array(
            'label' => t('Expected revenue'),
            'bundle' => 'opportunity',
        ),
        // CUSTOMER INSTANCES ==================================================
        'customer_first_name' => array(
            'label' => t('First name'),
            'bundle' => 'customer',
            'widget' => array('type' => 'text_textfield'),
        ),
        'customer_last_name' => array(
            'label' => t('Last name'),
            'bundle' => 'customer',
            'widget' => array('type' => 'text_textfield'),
        ),
        'customer_title' => array(
            'label' => t('Title'),
            'bundle' => 'customer',
            'widget' => array('type' => 'autocomplete_deluxe_taxonomy'),
        ),
        'customer_is_a_company' => array(
            'label' => t('Is a company'),
            'bundle' => 'customer',
        ),
        'customer_tags' => array(
            'label' => t('Tags'),
            'bundle' => 'customer',
            'widget' => array('type' => 'autocomplete_deluxe_taxonomy'),
        ),
        'customer_email' => array(
            'label' => t('E-mail'),
            'bundle' => 'customer',
            'widget' => array('type' => 'email_textfield'),
        ),
        'customer_phonenumber' => array(
            'label' => t('Phone'),
            'bundle' => 'customer',
            'widget' => array('type' => 'text_textfield'),
        ),
        'customer_mobilenumber' => array(
            'label' => t('Mobile'),
            'bundle' => 'customer',
            'widget' => array('type' => 'text_textfield'),
        ),
        'customer_fax' => array(
            'label' => t('Fax'),
            'bundle' => 'customer',
            'widget' => array('type' => 'text_textfield'),
        ),
        'customer_address' => array(
            'label' => t('Address'),
            'bundle' => 'customer',
            'widget' => array('type' => 'text_textarea'),
        ),
        'customer_url' => array(
            'label' => t('Website'),
            'bundle' => 'customer',
            'widget' => array('type' => 'url_external'),
        ),
        'customer_photo' => array(
            'label' => t('Photo'),
            'bundle' => 'customer',
            'widget' => array('type' => 'image_image'),
        ),
        'customer_job_position' => array(
            'label' => t('Job position'),
            'bundle' => 'customer',
            'widget' => array('type' => 'text_textfield'),
        ),
    );

    foreach ($instances as $key => &$instance) {
        $instance['field_name'] = $key;
        $instance['entity_type'] = 'node';
        $instance['locked'] = TRUE;
    }

    return $instances;
}

/**
 * Returns vocabularies
 */
function _erp_sales_vocabularies() {
    $vocabularies = array(
        array(
            'name' => st('Categories'),
            'machine_name' => 'categories',
            'hierarchy' => 0,
            'description' => st('Categoires for opportunity in ERP Sales module'),
        ),
        array(
            'name' => st('Customers tags'),
            'machine_name' => 'customer_tags',
            'hierarchy' => 0,
            'description' => st('Customers tags'),
        ),
        array(
            'name' => st('Titles'),
            'machine_name' => 'titles',
            'hierarchy' => 0,
            'description' => st('Peoples\' titles'),
        ),
    );
    return array_map('_erp_sales_array_to_object', $vocabularies);
}

function _erp_sales_array_to_object($array) {
    return (object) $array;
}